import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Product } from './interfaces/product.interface';

@Injectable({ providedIn: 'root' })
export class ProductHistoryService {
  subject: Subject<Product[]>;

  constructor() {
    this.subject = new Subject();
  }

  private getLocalProducts(): Product[] {
    return JSON.parse(localStorage.getItem('webapps-ng7-products')) || [];
  }

  public refresh(): void {
    this.subject.next(this.getLocalProducts());
  }

  public addToHistory(product: Product): void {
    const localProducts: Product[] = this.getLocalProducts();
    const localProductIDs: string[] = localProducts.map(product => product.id);
    const sameProductIndex: number = localProductIDs.indexOf(product.id);

    if (sameProductIndex > -1) {
      const tmp: Product = localProducts.splice(sameProductIndex, 1)[0];
      localProducts.push(tmp);
    } else {
      localProducts.push(product);
    }

    const productSet: Product[] = localProducts.slice(-4).reverse();
    localStorage.setItem('webapps-ng7-products', JSON.stringify(productSet));
    this.subject.next(productSet);
  }
}
