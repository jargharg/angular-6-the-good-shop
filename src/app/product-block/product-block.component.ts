import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../interfaces/product.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-block',
  templateUrl: './product-block.component.html',
  styleUrls: ['./product-block.component.scss'],
})
export class ProductBlockComponent {
  @Input() product: Product;

  constructor(private router: Router) {}

  goToProduct(product: Product): void {
    this.router.navigate(['/product', product.id]);
  }
}
