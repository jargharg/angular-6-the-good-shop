import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from './cart/cart.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(private router: Router, private cartService: CartService) {}

  goHome(): void {
    this.router.navigate(['/']);
  }

  closeCart(): void {
    this.cartService.changeVisible(false);
  }
}
