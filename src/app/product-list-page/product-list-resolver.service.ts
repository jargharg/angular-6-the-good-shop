import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Product } from '../interfaces/product.interface';
import { ProductsService } from '../products.service';

@Injectable({ providedIn: 'root' })
export class ProductListResolver implements Resolve<Product[]> {
  constructor(private productsService: ProductsService) {}

  public resolve(): Observable<Product[]> {
    return this.productsService.getProducts();
  }
}
