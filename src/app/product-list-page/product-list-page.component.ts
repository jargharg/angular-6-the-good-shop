import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { Product } from '../interfaces/product.interface';

@Component({
  selector: 'app-product-list-page',
  templateUrl: './product-list-page.component.html',
  styleUrls: ['./product-list-page.component.scss'],
})
export class ProductListPageComponent implements OnInit, OnDestroy {
  constructor(private route: ActivatedRoute) {}
  products: Product[] = [];
  routeSubscription: Subscription;

  ngOnInit(): void {
    this.routeSubscription = this.route.data.subscribe(({ data: products }) => {
      this.products = products;
    });
  }

  ngOnDestroy(): void {
    this.routeSubscription.unsubscribe();
  }
}
