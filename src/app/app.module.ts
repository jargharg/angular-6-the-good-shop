import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductListPageComponent } from './product-list-page/product-list-page.component';
import { ProductDetailPageComponent } from './product-detail-page/product-detail-page.component';
import { RecentlyViewedComponent } from './recently-viewed/recently-viewed.component';
import { ProductBlockComponent } from './product-block/product-block.component';
import { CartComponent } from './cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { CartSummaryComponent } from './checkout/cart-summary/cart-summary.component';
import { AddressComponent } from './checkout/address/address.component';
import { ConfirmComponent } from './checkout/confirm/confirm.component';
import { PaymentComponent } from './checkout/payment/payment.component';
import { CheckoutHeaderComponent } from './checkout/header/header.component';
import { OrderCompleteComponent } from './checkout/complete/complete.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductListPageComponent,
    ProductDetailPageComponent,
    RecentlyViewedComponent,
    ProductBlockComponent,
    CartComponent,
    CheckoutComponent,
    CartSummaryComponent,
    AddressComponent,
    ConfirmComponent,
    PaymentComponent,
    CheckoutHeaderComponent,
    OrderCompleteComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
