import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { CheckoutService } from '../checkout.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-checkout-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class CheckoutHeaderComponent implements OnInit, OnDestroy {
  routerSubscription: Subscription;
  addressRouteAvailable = true;
  paymentRouteAvailable = false;
  availableRoutes = { address: true, payment: false };

  constructor(private router: Router, private checkoutService: CheckoutService) {}

  ngOnInit(): void {
    this.checkValidRoutes();
    this.routerSubscription = this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) this.checkValidRoutes();
    });
  }

  ngOnDestroy(): void {
    this.routerSubscription.unsubscribe();
  }

  private checkValidRoutes(): void {
    if (this.router.url.includes('complete')) {
      this.addressRouteAvailable = false;
      this.paymentRouteAvailable = false;
    } else {
      this.addressRouteAvailable = true;
      console.log(this.checkoutService.addresses.billing);

      this.paymentRouteAvailable =
        this.checkoutService.addresses.billing.valid &&
        this.checkoutService.addresses.delivery.valid;
    }
  }
}
