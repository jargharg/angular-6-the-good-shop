import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { CheckoutService } from '../checkout.service';
import { Addresses } from 'src/app/interfaces/addresses.interface';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CartService } from 'src/app/cart/cart.service';
import { Cart } from 'src/app/interfaces/cart.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss'],
})
export class ConfirmComponent implements OnInit, OnDestroy {
  addresses: Addresses;
  cardDetails: FormGroup;
  cart: Cart;
  cartSubscription: Subscription;
  continueEnabled: boolean = false;

  constructor(
    private router: Router,
    private checkoutService: CheckoutService,
    private cartService: CartService,
  ) {
    this.cartSubscription = this.cartService.subject.subscribe(cart => {
      this.cart = cart;
    });
  }

  ngOnInit(): void {
    this.addresses = this.checkoutService.addresses;
    this.cardDetails = this.checkoutService.cardDetails;
  }

  ngOnDestroy(): void {
    this.cartSubscription.unsubscribe();
  }

  public onSubmit(): void {
    this.router.navigate(['/checkout/complete']);
  }
}
