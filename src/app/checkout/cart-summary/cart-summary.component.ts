import { Component, OnInit, OnDestroy } from '@angular/core';
import { Cart } from 'src/app/interfaces/cart.interface';
import { CartService } from 'src/app/cart/cart.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-cart-summary',
  templateUrl: './cart-summary.component.html',
  styleUrls: ['./cart-summary.component.scss'],
})
export class CartSummaryComponent implements OnInit, OnDestroy {
  cart: Cart;
  cartSubscription: Subscription;
  hideElements: boolean = false;
  routerSubscription: Subscription;
  total: number;

  constructor(private cartService: CartService, private router: Router) {
    this.cartSubscription = this.cartService.subject.subscribe((cart: Cart) => {
      this.cart = cart;
      if (this.cart.quantity === 0) this.router.navigate(['/']);
    });
  }

  ngOnInit(): void {
    this.updateHiddenElements();
    this.routerSubscription = this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) this.updateHiddenElements();
    });
    this.cartService.refresh();
  }

  ngOnDestroy(): void {
    this.cartSubscription.unsubscribe();
    this.routerSubscription.unsubscribe();
  }

  private updateHiddenElements(): void {
    this.hideElements = this.router.url.includes('complete');
  }

  public remove(id: string, size: string): void {
    this.cartService.removeFromCart(id, size);
  }
}
