import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Addresses } from '../interfaces/addresses.interface';

@Injectable({ providedIn: 'root' })
export class CheckoutService {
  addresses: Addresses = {
    delivery: this.formBuilder.group({
      name: ['', Validators.required],
      line1: ['', Validators.required],
      line2: [''],
      city: [''],
      postcode: ['', [Validators.required, Validators.minLength(6)]],
    }),
    billing: this.formBuilder.group({
      name: ['', Validators.required],
      line1: ['', Validators.required],
      line2: [''],
      city: [''],
      postcode: ['', [Validators.required, Validators.minLength(6)]],
    }),
  };

  cardDetails: FormGroup = this.formBuilder.group({
    number: ['', [Validators.required, Validators.pattern('[0-9]{16}')]],
    month: [null, [Validators.required, Validators.pattern('[0-1][0-9]')]],
    year: [null, [Validators.required, Validators.pattern('[0-9]{2}')]],
    name: ['', Validators.required],
    cvv: ['', [Validators.required, Validators.pattern('[0-9]{3}')]],
  });

  constructor(private formBuilder: FormBuilder) {}

  public reset(): void {
    this.addresses.delivery.reset();
    this.addresses.billing.reset();
    this.cardDetails.reset();
  }

  public updateCardDetails(cardDetails: FormGroup): void {
    this.cardDetails = cardDetails;
  }
}
