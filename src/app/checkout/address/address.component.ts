import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { CheckoutService } from '../checkout.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss'],
})
export class AddressComponent implements OnInit, OnDestroy {
  deliveryAddress: FormGroup;
  billingAddress: FormGroup;
  deliverySubscription: Subscription;
  billingSubscription: Subscription;
  continueEnabled: boolean = false;

  constructor(private router: Router, private checkoutService: CheckoutService) {}

  ngOnInit() {
    const { delivery, billing } = this.checkoutService.addresses;
    this.deliveryAddress = delivery;
    this.billingAddress = billing;
    this.continueEnabled = this.checkAddressesFilled();
    this.subscribeToAddressChanges();
  }

  ngOnDestroy(): void {
    this.deliverySubscription.unsubscribe();
    this.billingSubscription.unsubscribe();
  }

  private checkAddressesFilled(): boolean {
    return (
      this.deliveryAddress.status === 'VALID' && this.billingAddress.status === 'VALID'
    );
  }

  private subscribeToAddressChanges(): void {
    this.deliverySubscription = this.deliveryAddress.valueChanges.subscribe(() => {
      this.continueEnabled = this.checkAddressesFilled();
    });

    this.billingSubscription = this.billingAddress.valueChanges.subscribe(() => {
      this.continueEnabled = this.checkAddressesFilled();
    });
  }

  public copyDeliveryToBillingAddress(): void {
    this.billingAddress.setValue(this.deliveryAddress.value);
  }

  public onSubmit(): void {
    this.router.navigate(['checkout/payment']);
  }
}
