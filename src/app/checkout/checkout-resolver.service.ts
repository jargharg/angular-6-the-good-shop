import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';

import { CartService } from '../cart/cart.service';
import { Cart } from '../interfaces/cart.interface';

@Injectable({ providedIn: 'root' })
export class CheckoutResolver implements Resolve<Cart> {
  constructor(private cartService: CartService) {}

  public resolve(): Observable<Cart> {
    return of(this.cartService.cart);
  }
}
