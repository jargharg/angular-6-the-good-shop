import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CartService } from 'src/app/cart/cart.service';
import { CheckoutService } from '../checkout.service';

@Component({
  selector: 'app-complete',
  templateUrl: './complete.component.html',
  styleUrls: ['./complete.component.scss'],
})
export class OrderCompleteComponent implements OnInit {
  constructor(
    private router: Router,
    private cartService: CartService,
    private checkoutService: CheckoutService,
  ) {}

  ngOnInit() {}

  public onSubmit() {
    this.router.navigate(['/']);
    this.cartService.reset();
    this.checkoutService.reset();
  }
}
