import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { CheckoutService } from '../checkout.service';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit, OnDestroy {
  cardDetails: FormGroup;
  continueEnabled: boolean = false;
  cardDetailsSubscription: Subscription;

  constructor(private router: Router, private checkoutService: CheckoutService) {}

  ngOnInit(): void {
    this.cardDetails = this.checkoutService.cardDetails;
    this.subscribeToFormChanges();
  }

  ngOnDestroy(): void {
    this.cardDetailsSubscription.unsubscribe();
  }

  private checkFormFilled(): boolean {
    return true;
  }

  private subscribeToFormChanges(): void {
    this.cardDetailsSubscription = this.cardDetails.valueChanges.subscribe(() => {
      this.continueEnabled = this.checkFormFilled();
    });
  }

  public onSubmit(): void {
    this.router.navigate(['checkout/complete']);
  }
}
