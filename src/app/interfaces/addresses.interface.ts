import { FormGroup } from '@angular/forms';

export interface Addresses {
  billing: FormGroup;
  delivery: FormGroup;
}
