import { Product } from './product.interface';

export interface CartProduct {
  product: Product;
  size: string;
  quantity: number;
}
