import { CartProduct } from './cartProduct.interface';

export interface Cart {
  currency: string;
  delivery: number;
  products: CartProduct[];
  quantity: number;
  total: number;
  visible: boolean;
}
