import { Component, OnInit, OnDestroy } from '@angular/core';

import { Product } from '../interfaces/product.interface';
import { ProductHistoryService } from '../productHistory.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-recently-viewed',
  templateUrl: './recently-viewed.component.html',
  styleUrls: ['./recently-viewed.component.scss'],
})
export class RecentlyViewedComponent implements OnInit, OnDestroy {
  historySubscription: Subscription;
  products: Product[] = [];

  constructor(private productHistoryService: ProductHistoryService) {
    this.historySubscription = this.productHistoryService.subject.subscribe(
      products => (this.products = products),
    );
  }

  ngOnInit(): void {
    this.productHistoryService.refresh();
  }

  ngOnDestroy(): void {
    this.historySubscription.unsubscribe();
  }
}
