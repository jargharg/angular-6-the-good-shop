import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { Product } from '../interfaces/product.interface';
import { CartService } from '../cart/cart.service';

@Component({
  selector: 'app-product-detail-page',
  templateUrl: './product-detail-page.component.html',
  styleUrls: ['./product-detail-page.component.scss'],
})
export class ProductDetailPageComponent implements OnInit, OnDestroy {
  product: Product;
  selectedSize: string;
  routeSubscription: Subscription;

  constructor(private route: ActivatedRoute, private cartService: CartService) {}

  ngOnInit(): void {
    this.routeSubscription = this.route.data.subscribe(({ data: product }) => {
      this.product = product;
      this.selectSize('');
      window.scrollTo({ top: 0 });
    });
  }

  ngOnDestroy(): void {
    this.routeSubscription.unsubscribe();
  }

  public selectSize(size: string): void {
    this.selectedSize = size;
  }

  public addToBasket(size: string): void {
    this.cartService.addToCart({ product: this.product, quantity: 1, size });
    this.selectSize('');
  }
}
