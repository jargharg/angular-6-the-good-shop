import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { Product } from '../interfaces/product.interface';
import { ProductsService } from '../products.service';
import { ProductHistoryService } from '../productHistory.service';

@Injectable({ providedIn: 'root' })
export class ProductDetailResolver implements Resolve<Product> {
  constructor(
    private productsService: ProductsService,
    private productHistoryService: ProductHistoryService,
  ) {}

  public resolve(route: ActivatedRouteSnapshot): Observable<Product> {
    return this.productsService.getProduct(route.paramMap.get('id')).pipe(
      mergeMap(product => {
        this.productHistoryService.addToHistory(product);
        return of(product);
      }),
    );
  }
}
