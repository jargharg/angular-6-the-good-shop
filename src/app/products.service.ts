import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Product } from './interfaces/product.interface';

@Injectable({ providedIn: 'root' })
export class ProductsService {
  private productsURL: string =
    'https://s3-eu-west-1.amazonaws.com/api.themeshplatform.com/products.json';
  private products: Product[] = [];
  private lastFetch: number = 0;
  private cacheTime: number = 60 * 1000; // cache for 60 seconds

  constructor(private http: HttpClient) {}

  getProducts(): Observable<Product[]> {
    if (this.products.length > 0 && this.lastFetch > Date.now() - this.cacheTime)
      return of(this.products);

    return this.http.get<any>(this.productsURL).pipe(
      tap(response => {
        this.lastFetch = Date.now();
        this.products = response.data;
      }),
      map(({ data }) => data),
      catchError(this.handleError('getProducts', [])),
    );
  }

  // this would be a separate API call in a production app
  getProduct(productID: string): Observable<Product> {
    return this.getProducts().pipe(
      map(products => {
        for (let i = 0; i < products.length; i++) {
          if (products[i].id === productID) return products[i];
        }
        return null;
      }),
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
