import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { CartProduct } from '../interfaces/cartProduct.interface';
import { Cart } from '../interfaces/cart.interface';

@Injectable({ providedIn: 'root' })
export class CartService {
  subject: Subject<Cart>;
  cart: Cart;

  constructor() {
    this.subject = new Subject();
    this.cart = {
      products: this.getLocalCart(),
      visible: false,
      total: 0,
      quantity: 0,
      delivery: 5,
      currency: 'GBP',
    };
  }

  private getLocalCart(): CartProduct[] {
    return JSON.parse(localStorage.getItem('webapps-ng7-cart')) || [];
  }

  private saveToLocalStorage(): void {
    localStorage.setItem('webapps-ng7-cart', JSON.stringify(this.cart.products));
  }

  private findProductInCart(id: string, size: string): number | null {
    for (let i = 0; i < this.cart.products.length; i++) {
      if (
        this.cart.products[i].product.id === id &&
        this.cart.products[i].size === size
      ) {
        return i;
      }
    }

    return null;
  }

  private getTotal(): number {
    if (this.cart.products) {
      return this.cart.products.reduce((acc: number, product: CartProduct) => {
        return acc + product.quantity * +product.product.price.amount;
      }, 0);
    }
    return 0;
  }

  private getQuantity(): number {
    if (this.cart.products) {
      return this.cart.products.reduce((acc: number, product: CartProduct) => {
        return acc + +product.quantity;
      }, 0);
    }
    return 0;
  }

  public addToCart(product: CartProduct): void {
    const existingProductIndex: number = this.findProductInCart(
      product.product.id,
      product.size,
    );

    if (existingProductIndex !== null) {
      this.cart.products[existingProductIndex].quantity++;
    } else {
      this.cart.products.push(product);
    }

    this.cart.visible = true;
    this.saveToLocalStorage();
    this.refresh();
  }

  public changeVisible(visible: boolean): void {
    this.cart.visible = visible;
  }

  public refresh(): void {
    this.cart.quantity = this.getQuantity();
    this.cart.total = this.getTotal();
    this.subject.next(this.cart);
  }

  public removeFromCart(id: string, size: string): void {
    const existingProductIndex = this.findProductInCart(id, size);

    if (existingProductIndex !== null) {
      this.cart.products.splice(existingProductIndex, 1);
      this.saveToLocalStorage();
      this.refresh();

      if (this.cart.products.length === 0) this.cart.visible = false;
    }
  }

  public reset(): void {
    this.cart.products = [];
    this.saveToLocalStorage();
    this.refresh();
  }

  public toggleVisible(): void {
    if (this.cart.products.length > 0) this.cart.visible = !this.cart.visible;
  }
}
