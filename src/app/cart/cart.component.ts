import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { CartService } from './cart.service';
import { Cart } from '../interfaces/cart.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit, OnDestroy {
  cartSubscription: Subscription;
  cart: Cart;

  constructor(private cartService: CartService, private router: Router) {
    this.cartSubscription = this.cartService.subject.subscribe((cart: Cart) => {
      this.cart = cart;
    });
  }

  ngOnInit(): void {
    this.cartService.refresh();
  }

  ngOnDestroy(): void {
    this.cartSubscription.unsubscribe();
  }

  public goToCheckout(): void {
    this.router.navigate(['checkout']);
  }

  public goToProduct(id: string): void {
    this.router.navigate(['product', id]);
    this.cartService.toggleVisible();
  }

  public remove(id: string, size: string): void {
    this.cartService.removeFromCart(id, size);
  }

  public toggleCart(): void {
    this.cartService.toggleVisible();
  }
}
