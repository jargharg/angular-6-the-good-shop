import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductListPageComponent } from './product-list-page/product-list-page.component';
import { ProductDetailPageComponent } from './product-detail-page/product-detail-page.component';
import { ProductDetailResolver } from './product-detail-page/product-detail-resolver.service';
import { ProductListResolver } from './product-list-page/product-list-resolver.service';
import { CheckoutComponent } from './checkout/checkout.component';
import { CheckoutResolver } from './checkout/checkout-resolver.service';
import { AddressComponent } from './checkout/address/address.component';
import { PaymentComponent } from './checkout/payment/payment.component';
import { OrderCompleteComponent } from './checkout/complete/complete.component';

const routes: Routes = [
  { path: '', redirectTo: '/products', pathMatch: 'full' },
  {
    path: 'products',
    component: ProductListPageComponent,
    resolve: { data: ProductListResolver },
  },
  {
    path: 'product/:id',
    component: ProductDetailPageComponent,
    resolve: { data: ProductDetailResolver },
  },
  {
    path: 'checkout',
    component: CheckoutComponent,
    resolve: { data: CheckoutResolver },
    children: [
      { path: '', component: AddressComponent },
      { path: 'payment', component: PaymentComponent },
      { path: 'complete', component: OrderCompleteComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
